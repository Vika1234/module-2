import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Programm {
    public static void main(String[] args) throws IOException {
        try (FileWriter writer = new FileWriter("notes3.txt", false)) {
            String text = "Hello World!";
            writer.write(text);
            writer.append('\n');
            writer.flush();
            try (FileReader reader = new FileReader("notes3.txt")) {
                int c;
                while ((c = reader.read()) != -1) {
                    System.out.print((char) c);
                }
            }
        }
    }
}